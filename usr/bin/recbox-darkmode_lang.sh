#!/usr/bin/env bash

SYS_LANG=$(echo "$LANG" | cut -d '.' -f 1)
if [[ -z $(find /usr/share/recbox-darkmode/translations/settings/ -name "$SYS_LANG".trans) ]]; then
    source /usr/share/recbox-darkmode/translations/settings/en_US.trans
else
    for TRANS in /usr/share/recbox-darkmode/translations/settings/"$SYS_LANG"*; do
        source $TRANS
    done
fi
