#!/usr/bin/env bash

VER="1.3"

# Translation
source /usr/bin/recbox-darkmode_lang.sh

CONFIG_PATH="$HOME/.config/recbox-darkmode-settings/config"
DARK_MODE_STATUS=$(grep -w dark_mode_status "$CONFIG_PATH" | awk -F '=' '{print $2}')
if [[ -z $(find "$HOME/.config" -type d -name side-menu) ]]; then
    echo -e ":: sidemenu config don't exist\n -> skipping"
else
    SIDEMENU_CONFIG_PATH="$HOME/.config/side-menu/sidemenu-config.csv"
fi
ICON_PATH="/usr/share/icons/hicolor/64x64/apps/recbox-darkmode.svg"
QT_CONFIG_PATH="$HOME/.config/qt5ct/qt5ct.conf"

config_test() {
create_new_config() {
    touch "$CONFIG_PATH"
    if [[ -z $(find "$HOME/.config" -type d -name Kvantum) ]]; then
        echo -e "dark_mode_status=Off
font_icon_on=
font_icon_off=
gtk_theme_light=$(xfconf-query -c xsettings -p /Net/ThemeName -v)
gtk_theme_dark=$(xfconf-query -c xsettings -p /Net/ThemeName -v)
kvantum_theme_light=none
kvantum_theme_dark=none
icons_theme_light=$(xfconf-query -c xsettings -p /Net/IconThemeName -v)
icons_theme_dark=$(xfconf-query -c xsettings -p /Net/IconThemeName -v)
qt_icon_theme_light=$(grep -w icon_theme "$HOME/.config/qt5ct/qt5ct.conf" | awk -F '=' '{print $2}')
qt_icon_theme_dark=$(grep -w icon_theme "$HOME/.config/qt5ct/qt5ct.conf" | awk -F '=' '{print $2}')
cursor_theme_light=$(xfconf-query -c xsettings -p /Gtk/CursorThemeName -v)
cursor_theme_dark=$(xfconf-query -c xsettings -p /Gtk/CursorThemeName -v)
side_menurc=sidemenu-lightrc" > "$CONFIG_PATH"
    else
        echo -e "dark_mode_status=Off
font_icon_on=
font_icon_off=
gtk_theme_light=$(xfconf-query -c xsettings -p /Net/ThemeName -v)
gtk_theme_dark=$(xfconf-query -c xsettings -p /Net/ThemeName -v)
kvantum_theme_light=$(grep -w theme "$HOME/.config/Kvantum/kvantum.kvconfig" | awk -F '=' '{print $2}')
kvantum_theme_dark=$(grep -w theme "$HOME/.config/Kvantum/kvantum.kvconfig" | awk -F '=' '{print $2}')
icons_theme_light=$(xfconf-query -c xsettings -p /Net/IconThemeName -v)
icons_theme_dark=$(xfconf-query -c xsettings -p /Net/IconThemeName -v)
qt_icon_theme_light=$(grep -w icon_theme "$HOME/.config/qt5ct/qt5ct.conf" | awk -F '=' '{print $2}')
qt_icon_theme_dark=$(grep -w icon_theme "$HOME/.config/qt5ct/qt5ct.conf" | awk -F '=' '{print $2}')
cursor_theme_light=$(xfconf-query -c xsettings -p /Gtk/CursorThemeName -v)
cursor_theme_dark=$(xfconf-query -c xsettings -p /Gtk/CursorThemeName -v)
side_menurc=sidemenu-lightrc" > "$CONFIG_PATH"
    fi
}
    if [[ -z $(find "$HOME/.config" -type d -name recbox-darkmode-settings) ]]; then
        mkdir "$HOME/.config/recbox-darkmode-settings"
        create_new_config
    elif [[ -z $(find "$HOME/.config/recbox-nightlight-settings" -type f -name config) ]]; then
        create_new_config
    elif [[ ! -s $CONFIG_PATH ]]; then
        create_new_config
    fi
}

settings_sync() {
    if [[ $DARK_MODE_STATUS == "On" ]]; then
        sed -i "/gtk_theme_dark/ s/$(grep -w gtk_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')/$(xfconf-query -c xsettings -p /Net/ThemeName -v)/" "$CONFIG_PATH"
        if [[ -z $(find "$HOME/.config" -type d -name Kvantum) ]]; then
            echo ":: Kvantum Manager not installed or configured, skipping"
        else
            sed -i "/kvantum_theme_dark/ s/$(grep -w kvantum_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')/$(grep -w theme "$HOME/.config/Kvantum/kvantum.kvconfig" | awk -F '=' '{print $2}')/" "$CONFIG_PATH"
        fi
        sed -i "/icons_theme_dark/ s/$(grep -w icons_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')/$(xfconf-query -c xsettings -p /Net/IconThemeName -v)/" "$CONFIG_PATH"
        sed -i "/qt_icon_theme_dark/ s/$(grep -w qt_icon_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')/$(grep -w icon_theme "$HOME/.config/qt5ct/qt5ct.conf" | awk -F '=' '{print $2}')/" "$CONFIG_PATH"
        sed -i "/cursor_theme_dark/ s/$(grep -w cursor_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')/$(xfconf-query -c xsettings -p /Gtk/CursorThemeName -v)/" "$CONFIG_PATH"
    elif [[ $DARK_MODE_STATUS == "Off" ]]; then
        sed -i "/gtk_theme_light/ s/$(grep -w gtk_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')/$(xfconf-query -c xsettings -p /Net/ThemeName -v)/" "$CONFIG_PATH"
        if [[ -z $(find "$HOME/.config" -type d -name Kvantum) ]]; then
            echo ":: Kvantum Manager not installed or configured, skipping"
        else
            sed -i "/kvantum_theme_light/ s/$(grep -w kvantum_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')/$(grep -w theme "$HOME/.config/Kvantum/kvantum.kvconfig" | awk -F '=' '{print $2}')/" "$CONFIG_PATH"
        fi
        sed -i "/icons_theme_light/ s/$(grep -w icons_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')/$(xfconf-query -c xsettings -p /Net/IconThemeName -v)/" "$CONFIG_PATH"
        sed -i "/qt_icon_theme_light/ s/$(grep -w qt_icon_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')/$(grep -w icon_theme "$HOME/.config/qt5ct/qt5ct.conf" | awk -F '=' '{print $2}')/" "$CONFIG_PATH"
        sed -i "/cursor_theme_light/ s/$(grep -w cursor_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')/$(xfconf-query -c xsettings -p /Gtk/CursorThemeName -v)/" "$CONFIG_PATH"
    fi
}

settings() {
GTK_THEME_LIGHT=$(grep -w gtk_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')
GTK_THEME_DARK=$(grep -w gtk_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')
KVANTUM_THEME_LIGHT=$(grep -w kvantum_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')
KVANTUM_THEME_DARK=$(grep -w kvantum_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')
ICON_THEME_LIGHT=$(grep -w icons_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')
ICON_THEME_DARK=$(grep -w icons_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')
QT_ICON_THEME_LIGHT=$(grep -w qt_icon_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')
QT_ICON_THEME_DARK=$(grep -w qt_icon_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')
QT_CONFIG_THEME=$(grep -w icon_theme "$HOME/.config/qt5ct/qt5ct.conf" | awk -F '=' '{print $2}')
CURSOR_THEME_LIGHT=$(grep -w cursor_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')
CURSOR_THEME_DARK=$(grep -w cursor_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')

MAIN_MENU_TEXT="<b>$SETTINGS_GTK_THEMES:</b>\n<b>$SETTINGS_THEME_LIGHT: </b>$GTK_THEME_LIGHT    <b>$SETTINGS_THEME_DARK: </b>$GTK_THEME_DARK \
\n\n<b>$SETTINGS_KVANTUM_THEMES: </b>\n<b>$SETTINGS_THEME_LIGHT: </b>$KVANTUM_THEME_LIGHT    <b>$SETTINGS_THEME_DARK: </b>$KVANTUM_THEME_DARK \
\n\n<b>$SETTINGS_ICONS:</b>\n<b>$SETTINGS_THEME_LIGHT: </b>$ICON_THEME_LIGHT    <b>$SETTINGS_THEME_DARK: </b>$ICON_THEME_DARK \
\n\n<b>$SETTINGS_QTICONS:</b>\n<b>$SETTINGS_THEME_LIGHT: </b>$QT_ICON_THEME_LIGHT    <b>$SETTINGS_THEME_DARK: </b>$QT_ICON_THEME_DARK \
\n\n<b>$SETTINGS_CURSOR:</b>\n<b>$SETTINGS_THEME_LIGHT: </b>$CURSOR_THEME_LIGHT    <b>$SETTINGS_THEME_DARK: </b>$CURSOR_THEME_DARK\n"
PICK_COLUMN_TITLE="Pick"
THEME_COLUMN_TITLE="Theme"
MAIN_MENU="zenity --list --width=440 --height=485 --hide-header"
LIST_MENU="zenity --list --width=400 --height=458 --hide-header"

    MAIN_BOX=$($MAIN_MENU --title="$MAIN_MENU_TITLE" --text="$MAIN_MENU_TEXT" \
    --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
    --column "$THEME_COLUMN_TITLE" --window-icon="$ICON_PATH" \
    "$SETTINGS_GTK_LIGHT_ITEM" "$SETTINGS_GTK_DARK_ITEM" \
    "$SETTINGS_KVANTUM_LIGHT_ITEM" "$SETTINGS_KVANTUM_DARK_ITEM" \
    "$SETTINGS_ICONS_LIGHT_ITEM" "$SETTINGS_ICONS_DARK_ITEM" \
    "$SETTINGS_QT_ICONS_LIGHT_ITEM" "$SETTINGS_QT_ICONS_DARK_ITEM" \
    "$SETTINGS_CURSOR_LIGHT_ITEM" "$SETTINGS_CURSOR_DARK_ITEM" \
    "$SETTINGS_ABOUT_ITEM")

    if [[ -z $MAIN_BOX ]]; then
        exit 0
    elif [[ $MAIN_BOX == "$SETTINGS_GTK_LIGHT_ITEM" ]]; then
        GTK_BOX=$(find /usr/share/themes/*/gtk* $HOME/.themes/*/gtk* -type f -name gtk.css | awk -F '/' '{print $5}' | sort -u |
        $LIST_MENU --title="$LIST_GTK_LIGHT_TITLE" --text="$LIST_GTK_LIGHT_TEXT" \
        --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
        --extra-button="$EXTRA_BUTTON" --column="$PICK_COLUMN_TITLE" \
        --window-icon="$ICON_PATH" --separator=";")
            if [[ -z $GTK_BOX ]]; then
                exit 0
            elif [[ $GTK_BOX == "$EXTRA_BUTTON" ]]; then
                settings
            elif [[ $DARK_MODE_STATUS == "On" ]]; then
                sed -i "/gtk_theme_light/ s/$GTK_THEME_LIGHT/$GTK_BOX/" "$CONFIG_PATH"
                exec recbox-darkmode.sh -S
            elif [[ $DARK_MODE_STATUS == "Off" ]]; then
                sed -i "/gtk_theme_light/ s/$GTK_THEME_LIGHT/$GTK_BOX/" "$CONFIG_PATH"
                xfconf-query -c xsettings -p /Net/ThemeName -s "$GTK_BOX"
                xfconf-query -c xfwm4 -p /general/theme -s "$GTK_BOX"
                exec recbox-darkmode.sh -S
            else
                echo ":: GTK light menu error"
            fi
    elif [[ $MAIN_BOX == "$SETTINGS_GTK_DARK_ITEM" ]]; then
        GTK_BOX=$(find /usr/share/themes/*/gtk* $HOME/.themes/*/gtk* -type f -name gtk.css | awk -F '/' '{print $5}' | sort -u |
        $LIST_MENU --title="$LIST_GTK_DARK_TITLE" --text="$LIST_GTK_DARK_TEXT" \
        --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
        --extra-button="$EXTRA_BUTTON" --column="$PICK_COLUMN_TITLE" \
        --window-icon="$ICON_PATH" --separator=";")
            if [[ -z $GTK_BOX ]]; then
                exit 0
            elif [[ $GTK_BOX == "$EXTRA_BUTTON" ]]; then
                settings
            elif [[ $DARK_MODE_STATUS == "On" ]]; then
                sed -i "/gtk_theme_dark/ s/$GTK_THEME_DARK/$GTK_BOX/" "$CONFIG_PATH"
                xfconf-query -c xsettings -p /Net/ThemeName -s "$GTK_BOX"
                xfconf-query -c xfwm4 -p /general/theme -s "$GTK_BOX"
                exec recbox-darkmode.sh -S
            elif [[ $DARK_MODE_STATUS == "Off" ]]; then
                sed -i "/gtk_theme_dark/ s/$GTK_THEME_DARK/$GTK_BOX/" "$CONFIG_PATH"
                exec recbox-darkmode.sh -S
            else
                echo ":: GTK dark menu error"
            fi
    elif [[ $MAIN_BOX == "$SETTINGS_KVANTUM_LIGHT_ITEM" ]]; then
        if [[ -z $(find "$HOME/.config" -type d -name Kvantum) ]]; then
            zenity --info --width=400 --title="$INFO_WIDGET_TITLE" \
            --text="$INFO_WIDGET_TEXT" --window-icon="$ICON_PATH"
            exec recbox-darkmode.sh -S
        else
            KV_BOX=$(ls -d /usr/share/Kvantum/* | awk -F '/' '{print $5}' |
            $LIST_MENU --title="$LIST_KV_LIGHT_TITLE" --text="$LIST_KV_LIGHT_TEXT" \
            --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
            --extra-button="$EXTRA_BUTTON" --column="$PICK_COLUMN_TITLE" \
            --window-icon="$ICON_PATH" --separator=";")
                if [[ -z $KV_BOX ]]; then
                    exit 0
                elif [[ $KV_BOX == "$EXTRA_BUTTON" ]]; then
                    settings
                elif [[ $DARK_MODE_STATUS == "On" ]]; then
                    sed -i "/kvantum_theme_light/ s/$KVANTUM_THEME_LIGHT/$KV_BOX/" "$CONFIG_PATH"
                    exec recbox-darkmode.sh -S
                elif [[ $DARK_MODE_STATUS == "Off" ]]; then
                    sed -i "/kvantum_theme_light/ s/$KVANTUM_THEME_LIGHT/$KV_BOX/" "$CONFIG_PATH"
                    kvantummanager --set "$KV_BOX"
                    exec recbox-darkmode.sh -S
                else
                    echo ":: Kvantum light menu error"
                fi
        fi
    elif [[ $MAIN_BOX == "$SETTINGS_KVANTUM_DARK_ITEM" ]]; then
        if [[ -z $(find "$HOME/.config" -type d -name Kvantum) ]]; then
            zenity --info --width=400 --title="$INFO_WIDGET_TITLE" \
            --text="$INFO_WIDGET_TEXT" --window-icon="$ICON_PATH"
            exec recbox-darkmode.sh -S
        else
            KV_BOX=$(ls -d /usr/share/Kvantum/* | awk -F '/' '{print $5}' |
            $LIST_MENU --title="$LIST_KV_DARK_TITLE" --text="$LIST_KV_DARK_TEXT" \
            --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
            --extra-button="$EXTRA_BUTTON" --column="$PICK_COLUMN_TITLE" \
            --window-icon="$ICON_PATH" --separator=";")
                if [[ -z $KV_BOX ]]; then
                    exit 0
                elif [[ $KV_BOX == "$EXTRA_BUTTON" ]]; then
                    settings
                elif [[ $DARK_MODE_STATUS == "On" ]]; then
                    sed -i "/kvantum_theme_dark/ s/$KVANTUM_THEME_DARK/$KV_BOX/" "$CONFIG_PATH"
                    kvantummanager --set "$KV_BOX"
                    exec recbox-darkmode.sh -S
                elif [[ $DARK_MODE_STATUS == "Off" ]]; then
                    sed -i "/kvantum_theme_dark/ s/$KVANTUM_THEME_DARK/$KV_BOX/" "$CONFIG_PATH"
                    exec recbox-darkmode.sh -S
                else
                    echo ":: Kvantum dark menu error"
                fi
        fi
    elif [[ $MAIN_BOX == "$SETTINGS_ICONS_LIGHT_ITEM" ]]; then
        ICON_BOX=$(grep -E -s "Size" /usr/share/icons/*/index.theme $HOME/.icons/*/index.theme \
        $HOME/.local/share/icons/*/index.theme | awk -F '/' '{print $5}' | sort -u | grep -v hicolor |
        $LIST_MENU --title="$LIST_ICONS_LIGHT_TITLE" --text="$LIST_ICONS_LIGHT_TEXT" \
        --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
        --extra-button="$EXTRA_BUTTON" --column="$PICK_COLUMN_TITLE" \
        --window-icon="$ICON_PATH" --separator=";")
            if [[ -z $ICON_BOX ]]; then
                exit 0
            elif [[ $ICON_BOX == "$EXTRA_BUTTON" ]]; then
                settings
            elif [[ $DARK_MODE_STATUS == "On" ]]; then
                sed -i "/icons_theme_light/ s/$ICON_THEME_LIGHT/$ICON_BOX/" "$CONFIG_PATH"
                exec recbox-darkmode.sh -S
            elif [[ $DARK_MODE_STATUS == "Off" ]]; then
                sed -i "/icons_theme_light/ s/$ICON_THEME_LIGHT/$ICON_BOX/" "$CONFIG_PATH"
                xfconf-query -c xsettings -p /Net/IconThemeName -s "$ICON_BOX"
                exec recbox-darkmode.sh -S
            else
                echo ":: Icon light menu error"
            fi
    elif [[ $MAIN_BOX == "$SETTINGS_ICONS_DARK_ITEM" ]]; then
        ICON_BOX=$(grep -E -s "Size" /usr/share/icons/*/index.theme $HOME/.icons/*/index.theme \
        $HOME/.local/share/icons/*/index.theme | awk -F '/' '{print $5}' | sort -u | grep -v hicolor |
        $LIST_MENU --title="$LIST_ICONS_DARK_TITLE" --text="$LIST_ICONS_DARK_TEXT" \
        --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
        --extra-button="$EXTRA_BUTTON" --column="$PICK_COLUMN_TITLE" \
        --window-icon="$ICON_PATH" --separator=";")
            if [[ -z $ICON_BOX ]]; then
                exit 0
            elif [[ $ICON_BOX == "$EXTRA_BUTTON" ]]; then
                settings
            elif [[ $DARK_MODE_STATUS == "On" ]]; then
                sed -i "/icons_theme_dark/ s/$ICON_THEME_DARK/$ICON_BOX/" "$CONFIG_PATH"
                xfconf-query -c xsettings -p /Net/IconThemeName -s "$ICON_BOX"
                exec recbox-darkmode.sh -S
            elif [[ $DARK_MODE_STATUS == "Off" ]]; then
                sed -i "/icons_theme_dark/ s/$ICON_THEME_DARK/$ICON_BOX/" "$CONFIG_PATH"
                exec recbox-darkmode.sh -S
            else
                echo ":: Icon light menu error"
            fi
    elif [[ $MAIN_BOX == "$SETTINGS_QT_ICONS_LIGHT_ITEM" ]]; then
        QT_ICON_BOX=$(grep -E -s "Size" /usr/share/icons/*/index.theme $HOME/.icons/*/index.theme \
        $HOME/.local/share/icons/*/index.theme | awk -F '/' '{print $5}' | sort -u | grep -v hicolor |
        $LIST_MENU --title="$LIST_QT_ICONS_LIGHT_TITLE" --text="$LIST_QT_ICONS_LIGHT_TEXT" \
        --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
        --extra-button="$EXTRA_BUTTON" --column="$PICK_COLUMN_TITLE" \
        --window-icon="$ICON_PATH" --separator=";")
            if [[ -z $QT_ICON_BOX ]]; then
                exit 0
            elif [[ $QT_ICON_BOX == "$EXTRA_BUTTON" ]]; then
                settings
            elif [[ $DARK_MODE_STATUS == "On" ]]; then
                sed -i "/qt_icon_theme_light/ s/$QT_ICON_THEME_LIGHT/$QT_ICON_BOX/" "$CONFIG_PATH"
                exec recbox-darkmode.sh -S
            elif [[ $DARK_MODE_STATUS == "Off" ]]; then
                sed -i "/qt_icon_theme_light/ s/$QT_ICON_THEME_LIGHT/$QT_ICON_BOX/" "$CONFIG_PATH"
                sed -i "/icon_theme/ s/$QT_CONFIG_THEME/$QT_ICON_BOX/" "$QT_CONFIG_PATH"
                exec recbox-darkmode.sh -S
            else
                echo ":: Qt Icon light menu error"
            fi
    elif [[ $MAIN_BOX == "$SETTINGS_QT_ICONS_DARK_ITEM" ]]; then
        QT_ICON_BOX=$(grep -E -s "Size" /usr/share/icons/*/index.theme $HOME/.icons/*/index.theme \
        $HOME/.local/share/icons/*/index.theme | awk -F '/' '{print $5}' | sort -u | grep -v hicolor |
        $LIST_MENU --title="$LIST_QT_ICONS_DARK_TITLE" --text="$LIST_QT_ICONS_DARK_TEXT" \
        --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
        --extra-button="$EXTRA_BUTTON" --column="$PICK_COLUMN_TITLE" \
        --window-icon="$ICON_PATH" --separator=";")
            if [[ -z $QT_ICON_BOX ]]; then
                exit 0
            elif [[ $QT_ICON_BOX == "$EXTRA_BUTTON" ]]; then
                settings
            elif [[ $DARK_MODE_STATUS == "On" ]]; then
                sed -i "/qt_icon_theme_dark/ s/$QT_ICON_THEME_DARK/$QT_ICON_BOX/" "$CONFIG_PATH"
                sed -i "/icon_theme/ s/$QT_CONFIG_THEME/$QT_ICON_BOX/" "$QT_CONFIG_PATH"
                exec recbox-darkmode.sh -S
            elif [[ $DARK_MODE_STATUS == "Off" ]]; then
                sed -i "/qt_icon_theme_dark/ s/$QT_ICON_THEME_DARK/$QT_ICON_BOX/" "$CONFIG_PATH"
                exec recbox-darkmode.sh -S
            else
                echo ":: Qt Icon dark menu error"
            fi
    elif [[ $MAIN_BOX == "$SETTINGS_CURSOR_LIGHT_ITEM" ]]; then
        ICON_BOX=$(find /usr/share/icons/*/ $HOME/.icons/*/ \
        $HOME/.local/share/icons/*/ -type d -name cursors | awk -F '/' '{print $5}' |
        $LIST_MENU --title="$LIST_CURSOR_LIGHT_TITLE" --text="$LIST_CURSOR_LIGHT_TEXT" \
        --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
        --extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" \
        --window-icon="$ICON_PATH" --separator=";")
            if [[ -z $ICON_BOX ]]; then
                exit 0
            elif [[ $ICON_BOX == "$EXTRA_BUTTON" ]]; then
                settings
            elif [[ $DARK_MODE_STATUS == "On" ]]; then
                sed -i "/cursor_theme_light/ s/$CURSOR_THEME_LIGHT/$ICON_BOX/" "$CONFIG_PATH"
                exec recbox-darkmode.sh -S
            elif [[ $DARK_MODE_STATUS == "Off" ]]; then
                sed -i "/cursor_theme_light/ s/$CURSOR_THEME_LIGHT/$ICON_BOX/" "$CONFIG_PATH"
                xfconf-query -c xsettings -p /Gtk/CursorThemeName -s "$ICON_BOX"
                exec recbox-darkmode.sh -S
            else
                echo ":: Cursor light menu error"
            fi
    elif [[ $MAIN_BOX == "$SETTINGS_CURSOR_DARK_ITEM" ]]; then
        ICON_BOX=$(find /usr/share/icons/*/ $HOME/.icons/*/ \
        $HOME/.local/share/icons/*/ -type d -name cursors | awk -F '/' '{print $5}' |
        $LIST_MENU --title="$LIST_CURSOR_DARK_TITLE" --text="$LIST_CURSOR_DARK_TEXT" \
        --ok-label="$OK_BUTTON" --cancel-label="$CANCEL_BUTTON" \
        --extra-button="$EXTRA_BUTTON" --column="$COLUMN_TITLE" \
        --window-icon="$ICON_PATH" --separator=";")
            if [[ -z $ICON_BOX ]]; then
                exit 0
            elif [[ $ICON_BOX == "$EXTRA_BUTTON" ]]; then
                settings
            elif [[ $DARK_MODE_STATUS == "On" ]]; then
                sed -i "/cursor_theme_dark/ s/$CURSOR_THEME_DARK/$ICON_BOX/" "$CONFIG_PATH"
                xfconf-query -c xsettings -p /Gtk/CursorThemeName -s "$ICON_BOX"
                exec recbox-darkmode.sh -S
            elif [[ $DARK_MODE_STATUS == "Off" ]]; then
                sed -i "/cursor_theme_dark/ s/$CURSOR_THEME_DARK/$ICON_BOX/" "$CONFIG_PATH"
                exec recbox-darkmode.sh -S
            else
                echo ":: Cursor light menu error"
            fi
    elif [[ $MAIN_BOX == "$SETTINGS_ABOUT_ITEM" ]]; then
        zenity --info --no-wrap --icon-name="recbox-darkmode-settings" \
        --window-icon="$ICON_PATH" --title="$ABOUT_WIDGET_TITLE" \
        --text="<b>Version: $VER</b>\n\n$ABOUT_WIDGET_TEXT"
        exec recbox-darkmode.sh -S
    else
        echo ":: Main menu: error"
    fi

}

run_test() {
config_error_dialog() {
    zenity --error --width=400 --title="$CONFIG_ERROR_WIDGET_TITLE" --text="$CONFIG_ERROR_WIDGET_TEXT" \
    --window-icon="$ICON_PATH"
}
    if [[ -z $(find "$HOME/.config" -type d -name recbox-darkmode-settings) ]]; then
        config_error_dialog
    elif [[ -z $(find "$HOME/.config/recbox-nightlight-settings" -type f -name config) ]]; then
        config_error_dialog
    elif [[ ! -s $CONFIG_PATH ]]; then
        config_error_dialog
    else
        run
    fi
}

run() {
GTK_THEME_LIGHT=$(grep -w gtk_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')
GTK_THEME_DARK=$(grep -w gtk_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')
KVANTUM_THEME_LIGHT=$(grep -w kvantum_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')
KVANTUM_THEME_DARK=$(grep -w kvantum_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')
ICON_THEME_LIGHT=$(grep -w icons_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')
ICON_THEME_DARK=$(grep -w icons_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')
QT_ICON_THEME_LIGHT=$(grep -w qt_icon_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')
QT_ICON_THEME_DARK=$(grep -w qt_icon_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')
QT_CONFIG_THEME=$(grep -w icon_theme "$HOME/.config/qt5ct/qt5ct.conf" | awk -F '=' '{print $2}')
CURSOR_THEME_LIGHT=$(grep -w cursor_theme_light "$CONFIG_PATH" | awk -F '=' '{print $2}')
CURSOR_THEME_DARK=$(grep -w cursor_theme_dark "$CONFIG_PATH" | awk -F '=' '{print $2}')
if [[ -z $(find "$HOME/.config" -type d -name side-menu) ]]; then
    echo -e ":: sidemenu not installed\n -> skipping"
else
    FONT_ICON_ON=$(grep -w font_icon_on "$CONFIG_PATH" | awk -F '=' '{print $2}')
    FONT_ICON_OFF=$(grep -w font_icon_off "$CONFIG_PATH" | awk -F '=' '{print $2}')
fi

    if [[ $DARK_MODE_STATUS == "On" ]]; then
        xfconf-query -c xsettings -p /Net/ThemeName -s "$GTK_THEME_LIGHT"
        xfconf-query -c xfwm4 -p /general/theme -s "$GTK_THEME_LIGHT"
        if [[ $KVANTUM_THEME_LIGHT == "none" ]]; then
            echo ":: Kvantum Manager not installed or configured, skipping"
        else
            kvantummanager --set "$KVANTUM_THEME_LIGHT"
        fi
        xfconf-query -c xsettings -p /Net/IconThemeName -s "$ICON_THEME_LIGHT"
        xfconf-query -c xsettings -p /Gtk/CursorThemeName -s "$CURSOR_THEME_LIGHT"
        sed -i "/icon_theme/ s/$QT_CONFIG_THEME/$QT_ICON_THEME_LIGHT/" "$QT_CONFIG_PATH"
        if [[ -z $(find "$HOME/.config" -type d -name side-menu) ]]; then
            echo -e ":: sidemenu not installed\n -> skipping"
        else
            sed -i '/side_menurc/ s/sidemenu-darkrc/sidemenu-lightrc/' "$CONFIG_PATH"
            sed -i "s/$FONT_ICON_ON/$FONT_ICON_OFF/" "$SIDEMENU_CONFIG_PATH"
        fi
        sed -i '/dark_mode_status/ s/On/Off/' "$CONFIG_PATH"
        notify-send  --icon=recbox-darkmode -t 3000 "Dark Mode Off"
    elif [[ $DARK_MODE_STATUS == "Off" ]]; then
        xfconf-query -c xsettings -p /Net/ThemeName -s "$GTK_THEME_DARK"
        xfconf-query -c xfwm4 -p /general/theme -s "$GTK_THEME_DARK"
        if [[ $KVANTUM_THEME_DARK == "none" ]]; then
            echo ":: Kvantum Manager not installed or configured, skipping"
        else
            kvantummanager --set "$KVANTUM_THEME_DARK"
        fi
        xfconf-query -c xsettings -p /Net/IconThemeName -s "$ICON_THEME_DARK"
        xfconf-query -c xsettings -p /Gtk/CursorThemeName -s "$CURSOR_THEME_DARK"
        sed -i "/icon_theme/ s/$QT_CONFIG_THEME/$QT_ICON_THEME_DARK/" "$QT_CONFIG_PATH"
        if [[ -z $(find "$HOME/.config" -type d -name side-menu) ]]; then
            echo -e ":: sidemenu not installed\n -> skipping"
        else
            sed -i '/side_menurc/ s/sidemenu-lightrc/sidemenu-darkrc/' "$CONFIG_PATH"
            sed -i "s/$FONT_ICON_OFF/$FONT_ICON_ON/" "$SIDEMENU_CONFIG_PATH"
        fi
        sed -i '/dark_mode_status/ s/Off/On/' "$CONFIG_PATH"
        notify-send  --icon=recbox-darkmode -t 3000 "Dark Mode On"
    fi
}

error_dialog() {
    zenity --error --title="$ERROR_WIDGET_TITLE" --text="$ERROR_WIDGET_TEXT" \
    --no-wrap --window-icon="$ICON_PATH"
}

case "$1" in

    --settings-sync | -s ) settings_sync;;
    --settings | -S ) settings;;
    -sS ) config_test; settings_sync && settings || error_dialog;;
    --run | -r ) run_test;;
    --version | -v ) echo -e "\n    Version $VER\n";;
    *)

echo -e "
    Usage:\n
    recbox-darkmode.sh [ OPTION ]\n
    Options:\n
     -s, --settings-sync    Synchronizing themes settings with config file
                            in ~/.config/in dark-mode-settings/ directory.
     -S, --settings         Open Settings GUI.
     -r, --run              Run Redshift in One-Shot mode.
     -v, --version
" >&2
exit 1;;
esac
exit 0
