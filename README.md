## About:

RecBox Dark Mode is a simple theme switcher for XFCE desktop environment.

![darkmode](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-darkmode.gif)

## Installation:

#### Manjaro:
```
git clone https://gitlab.com/recbox/recbox-toolkit-bash/recbox-darkmode.git
cd recbox-darkmode
sudo cp -r sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity
```

## Translation
Directory containing translations can be found here:
* /usr/share/recbox-darkmode
* /usr/share/applications/recbox-darkmode.desktop
* /usr/share/applications/recbox-darkmode-settings.desktop

#### Translations:
* [x] en_US
* [x] pl_PL

## Translation contribution:
* type `echo $LANG` in terminal to check language used by your system
* copy `en_US.trans` file and rename it (replace `.utf8` with `.trans`)
* replace text under quotation marks
